from twisted.internet import reactor
from twisted.web.resource import Resource
from twisted.web.server import Site

import cgi

class FormPage(Resource):
	isLeaf=True
	def render_GET(self,request):
		return """
		<html>
		<body>
		<form method='post' >
		<input name="txt" />
		<input type="submit" name="sub" />
		</form>
		</body>
		</html>"""

	def render_POST(self,request):
		print request.args
		return """
		<html>
		<body>
		you Submitted %s
		</body>
		</html>""" %(cgi.escape(request.args["txt"][0]))

factory=Site(FormPage())
reactor.listenTCP(8000,factory)
reactor.run()
