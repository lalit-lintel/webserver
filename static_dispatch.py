from twisted.internet import reactor
from twisted.web.server import Site
from twisted.web.static import File

root = File('/var/www')
root.putChild("doc",File('/var'))
factory = Site(root)
reactor.listenTCP(8000,factory)
reactor.run()
